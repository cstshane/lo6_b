<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Select a supplier to update</title>
        
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">
            <h1>Update a Supplier</h1>
            <p>Pick a supplier to update:</p>
<?php
require_once __DIR__ . '/Autoloader.php';
$loader = new Autoloader();
$loader->addNamespaceMapping("\\CSTClasses_B",
        __DIR__ . '/../../private/CSTClasses_B' );
$loader->addNamespaceMapping( "\\Formitron", "classes/Formitron" );

use CSTClasses_B\DbObject;
use Formitron\BaseForm;
use Formitron\Element\Select;
use Formitron\Helpers;

// Connect to the database
$db = new DbObject();

// Query the database for the suppliers
$qryResults = $db->select( "SupplierID, CompanyName", "Suppliers",
        "", "CompanyName" );

// Build the option list
$supplierOptions = DbObject::createArray( $qryResults );

// We're done with the results -- free them
$qryResults->free();

// Create the supplier selection form
$form = new BaseForm( BaseForm::METHOD_POST, "6-DisplaySupplier.php",
        ["id"=>"supplierForm"] );

$supplierSelect = new Select( "SupplierID", $supplierOptions, "" );
$form->add( Helpers::withLabel( "SupplierID", "Supplier", $supplierSelect ) );

$form->add( Helpers::submitBlock( "submit", "Submit", "Reset" ) );
echo $form->render();

?>
        </div>
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </body>
</html>
