<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>List of all products</title>
    </head>
    <body>
        <h1>List of all products</h1>
        
<?php

require_once __DIR__ . '/Autoloader.php';
$loader = new Autoloader();
$loader->addNamespaceMapping( "\\CSTClasses_B",
        __DIR__ . '/../../private/CSTClasses_B' );

use CSTClasses_B\DbObject;

// Connect to the database
$db = new DbObject();

// Prepare the query statement.  Note: we're not using any bound parameters.
$qryStmt = "SELECT p.ProductID, p.ProductName, c.CategoryName " .
    "FROM Products p " .
    "JOIN Categories c ON p.CategoryID = c.categoryID " .
    "ORDER BY p.ProductID;";
$preparedStmt = $db->prepare( $qryStmt );

// Execute the query
$preparedStmt->execute();

// Grab all of the results from the MySQL server, and buffer them in the
// PHP client.
$preparedStmt->store_result();

// Specify which variables will contain which columns.
$preparedStmt->bind_result( $productID, $productName, $categoryName );

// Display the results in a table
echo "<table>\n";
echo " <thead>\n";
echo "  <tr><th>Product ID</th><th>Product Name</th><th>Category</th></tr>\n";
echo " </thead>\n";

echo " <tbody>\n";
while ( $preparedStmt->fetch() )
{
    echo "  <tr><td>$productID</td><td>$productName</td>" .
            "<td>$categoryName</td></tr>\n";
}
echo " </tbody>\n";
echo "</table>\n";

$preparedStmt->close();

?>
    </body>
</html>
