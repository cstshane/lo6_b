<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>New product addition - Confirmation</title>
    </head>
    <body>
        <h1>New product addition</h1>
        
<?php

require_once __DIR__ . '/Autoloader.php';
$loader = new Autoloader();
$loader->addNamespaceMapping("\\CSTClasses_B",
        __DIR__ . '/../../private/CSTClasses_B' );

use CSTClasses_B\DbObject;

// echo "<p>The specified supplier ID is {$_POST['SupplierID']}</p>\n";
// echo "<p>The specified category ID is {$_POST['CategoryID']}</p>\n";

if ( !isset( $_POST["Discontinued"] ) )
{
    $_POST["Discontinued"] = 0;
}

unset( $_POST["submit"] );
unset( $_POST["_submitted"] );

//foreach ( $_POST as $field=>$value )
//{
//    echo "<p>The specified value of $field is $value</p>\n";
//}

// Connect to the database
$db = new DbObject();

// Add the new product to the database
// $affectedRows = $db->insert( $_POST, "Products" );

// Create the query string, using ? for placeholders for variables that will
// be bound to the statement
$query = "INSERT INTO Products " .
        "( ProductName, QuantityPerUnit, UnitPrice, UnitsInStock, " .
        "UnitsOnOrder, ReorderLevel, SupplierID, CategoryID, " .
        "Discontinued ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ? )";

// Prepare the statement
$preparedStmt = $db->prepare( $query );

// Bind the varaibles to the placeholders in the query string
$preparedStmt->bind_param( "ssdiiiiii", $_POST["ProductName"], $qpu, $unitPrice,
        $unitsInStock, $unitsOnOrder, $reorderLevel, $supplierID,
        $categoryID, $discontinued );

// Now, assign the values to be inserted into those bound variables
// $productName = $_POST["ProductName"];
$qpu = $_POST["QuantityPerUnit"];
$unitPrice = $_POST["UnitPrice"];
$unitsInStock = $_POST["UnitsInStock"];
$unitsOnOrder = $_POST["UnitsOnOrder"];
$reorderLevel = $_POST["ReorderLevel"];
$supplierID = $_POST["SupplierID"];
$categoryID = $_POST["CategoryID"];
$discontinued = $_POST["Discontinued"];

// Execute the prepared statement
$preparedStmt->execute();

// Determine how many rows were inserted
$affectedRows = $preparedStmt->affected_rows;

// All done with the prepared statement -- free up the resources
$preparedStmt->close();

// Indicate to the user whether the insertion was successful
if ( $affectedRows > 0 )
{
    echo "<p>The new product, " . $_POST["ProductName"] .
            ", has been successfully added.</p>\n";
    echo "<p><a href='6-NewProduct.php'>Click here</a> " .
            "to add another product.</p>\n";
}
else
{
    echo "<p>" . $_POST["ProductName"] . " was not inserted.</p>\n";
    print "<p><a href='#' onclick='history.back(); return false;'>" .
            "Click here</a> to try to add that product again.</p>\n";
}

?>
    </body>
</html>
