<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Formitron\Element;


use Formitron\Element\BaseElement;
/**
 * Represents a file upload control.
 *
 * @author ins208
 */
class File extends BaseElement
{
    /**
     * A callable which will be called when a file has been uploaded, to provide
     * the user with an opportunity to handle the uploaded file
     * @var type callable
     */
    protected $handler;
    
    /**
     * Creates an instance of a file input element
     * @param string $name The value for the name property of this form item
     * @param callable The php callable (anonymous function, etc) to be called
     * when a file has been uploaded.
     * @param array $properties Extra properties for this element's tag
     */
    public function __construct( $name, callable $handler, $properties = array())
    {
	$this->handler = $handler;
	
	if(!isset($properties['id']))
	{
	    $properties['id']=$name;
	}	
	$properties['name'] = $name;
		
	$properties['type'] = "file";
		
	parent::__construct("input", $properties);
    }
    
    /**
     * Handles form submission for this item
     * @param type $formData GET or POST superglobal, depending on how the BaseForm
     * containing this item was configured
     */
    public function handleSubmit($formData)
    {
	$name = $this->properties['name'];
	
	if(isset($_FILES[$name]))
	{
	    //Call the user provided callable, passing in information about the
	    // uploaded file and the name of the file input element
	    call_user_func_array($this->handler, [$name, $_FILES[$name]]);
	}
    }


}
