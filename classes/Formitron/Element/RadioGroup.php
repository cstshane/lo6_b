<?php
namespace Formitron\Element;

use Formitron\Element\Group;

/**
 * Implementation of a group of radio buttons.
 * Essentially a wrapper, taking in a list of radio button labels and values,
 * creating a set of radio buttons all with the same name. Wraps each radio
 * button with a containerlabel for that radio
 *
 * @author ins208
 */
class RadioGroup extends Group
{
    protected $label;
    public function __construct($name, $label, $checkedValue, $options=array())
    {
	$this->label = $label;
	$this->wrapClass = "form-group radio-group";
	foreach($options as $label=>$value)
	{
	    $itemGroup = new Group();
	    
	    $identifier = $name . preg_replace("/[^a-zA-Z0-9]/", '', $value);

	    
	    //Bootstrap requires radio butons to be wrapped with <div class="radio">
	    $itemGroup->wrapGroupWith("div", "radio"); 
	    
	    //In radio groups, all radio buttons have the same name, but should have
	    // different IDs so that they can be uniquely identified
	    $rad = new Radio($name, $value, $checkedValue == $value,array('id'=>$identifier));
	    
	    $label = new ContainerLabel($identifier, $rad, $label);
	    $itemGroup->add($label);
	    
	    $this->add($itemGroup);
	}
	
	parent::__construct();
    }
    
    protected function renderInnerHTML()
    {
	
	return "\n<strong>{$this->label}</strong><br />" . parent::renderInnerHTML();
    }

    
}
