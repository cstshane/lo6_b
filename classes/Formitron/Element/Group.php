<?php
namespace Formitron\Element;

use Formitron\Element\BaseElement;
/**
 * A form element which contains other form elements. In this case, any
 * elements added to this Group will be rendered within opening and closing
 * div tags. Since it is also a BaseElement, it can be used as such, and
 * must dispatch any element calls on to its child elements.
 *
 * @author ins208
 */
class Group extends BaseElement
{
    protected $elements=array();
    
    protected $wrapTag="div";
    protected $wrapClass="form-group";
    
    public function __construct()
    {
	///NOTE:Use this to demonstrate variable length argument lists
	$args = func_get_args();
	foreach($args as $arg)
	{
	    if($arg instanceof BaseElement)
	    {
		$this->elements[] = $arg;
	    }
	}
	parent::__construct($this->wrapTag, ["class"=>$this->wrapClass]);
    }
    
    /**
     * Configure the tag which will wrap around all of the child elements.
     * @param string $tag The tag name for the wrapper
     * @param string $class The class(es) for the wrapper
     * @param array $properties Extra properties for the wrapper
     */
    public function wrapGroupWith($tag, $class, $properties= array())
    {
	$this->tag = $tag;
	$this->wrapClass = $class;
	$this->properties = array_merge($properties, ['class'=>$class]);
    }
    
    /**
     * Add another element to the set of children
     * @param BaseElement $element
     */
    public function add(BaseElement $element)
    {
	$this->elements[] = $element;
    }
    
    /**
     * Generate the inner HTML of this tag. The inner HTML consists of the
     * representation of each inner element in turn.
     * @return type
     */
    protected function renderInnerHTML()
    {
	$output = "\n";
	foreach($this->elements as $element)
	{
	    $output .= $element->render() . "\n";
	}
	return $output;
    }
    
    public function canHaveShortTag()
    {
	return false;
    }
    
    /**
     * Dispatch loadvalue requests to each sub-item
     * @param type $formData GET or POST superglobal
     */
    public function handleSubmit($formData)
    {
	foreach($this->elements as $element)
	{
	    $element->handleSubmit($formData);
	}
    }
}
