<?php
namespace Formitron\Element;

use Formitron\Element\BaseElement;
/**
 * Represents a HTML form select element.
 *
 * @author ins208
 */
class Select extends BaseElement
{
    protected $options;
    protected $selectedIndex;
    
    /**
     * Creates a HTML select element with the given set of options
     * @param type $name Name of the form element
     * @param type $options Array of options, where keys will be used as the "value"
     *	attribute of the option, and the value will be used as the label.
     * @param type $selectedindex The key of the item in $options which will be
     *	selected by default
     * @param array $properties
     */
    public function __construct($name, $options, $selectedindex, $properties = array())
    {
	if(!isset($properties['id']))
	{
	    $properties['id']=$name;
	}	
	$properties['name'] = $name;
	
	$properties['class'] = "form-control";
	
	$this->options = $options;
	
	$this->selectedIndex = $selectedindex;
	
	parent::__construct("select", $properties);
    }
    
    /**
     * Render the set of options as option tags.
     * Each option in the options array will be formatted as:
     *	<option value='$key'>$value</option>
     * @return type A string representation of this item's options
     */
    protected function renderInnerHTML()
    {
	$generatedElements = "\n";
	foreach($this->options as $value=>$label)
	{
	    $selected="";
	    if($value == $this->selectedIndex)
	    {
		$selected="selected";
	    }
	    $generatedElements .= "\t<option value=\"{$value}\" {$selected}>$label</option>\n";
	}
	return $generatedElements;
    }
    
    public function handleSubmit($formData)
    {
	if(isset($formData[$this->properties['name']]))
	{
	    $this->selectedIndex = $formData[$this->properties['name']];
	}
    }
}
