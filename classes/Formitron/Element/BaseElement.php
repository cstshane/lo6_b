<?php
namespace Formitron\Element;
/**
 * Description of BaseElement
 *
 * @author ins208
 */
abstract class BaseElement
{
    protected $tag;
    protected $properties = array();
    
    public function __construct($tag, $properties=array())
    {
	$this->tag = $tag;
	$this->properties = $properties;
    }
    
    public function render()
    {
	return $this->renderSelf();
    }
    
    /**
     * Generates an HTML representation of this form element. Default implementation
     * will render the given tag with $this->properties as attributes of that tag.
     * If canHaveShortTag returns true, and there is no content returned by 
     * renderInnerHTML, then the tag will use a XHTML /> close. If renderInnerHTML
     * returns a value, then that value will be inserted within the opening and 
     * closing tags for this element.
     * <{$tag} property1="value1" property2="value2">
     *   {$this->renderInnerHTML()}
     * </{$tag}>
     * @return string An HTML representation of this element
     */
    protected function renderSelf()
    {
	//Start the HTML tag
	$elementString = "<{$this->tag} ";
	//Generate the string to represent the element's tag properties, 
	// as key="value" pairs
	foreach($this->properties as $name=>$value)
	{    
	    $escapedValue = htmlspecialchars($value);
	    $elementString .= "${name}=\"${escapedValue}\" ";
	}
	
	
	$contentWithin = $this->renderInnerHTML();
	if(empty($contentWithin) && $this->canHaveShortTag())
	{
	    $elementString .= "/>\n";
	}
	else
	{
	    $elementString .= ">";
	    $elementString .= $contentWithin;
	    $elementString .= "</{$this->tag}>\n";

	}
	return $elementString;
    }
    
    /**
     * 
     * @return boolean True if this element is able to use a shorthand closing tag
     * ie <input type=.... />
     */
    protected function canHaveShortTag()
    {
	return true;
    }
    
    /**
     * Generate the content to be placed within this elements opening and closing tags.
     * @return string The string to be placed within this elements opening and closing tags.
     */
    protected function renderInnerHTML()
    {
	return '';
    }
    
 
    
    //NOTE: Showing how to use magic methods
    /**
     * __call is a "magic" method which is called, if present, when a method
     * which is not defined in an object is called on an object. 
     * @param type $methodName The string name of the method which was called
     * @param type $args An array of the arguments, in order, which were passed
     * to the method call.
     */
    public function __call($methodName, $args)
    {
	//We're going to define two __call magic methods. If setSomevalue is called,
	// we want to set this->properties[somevalue] to the value of the first
	// argument. If addSomevalue is called, we want to append the first argument
	// to the current value of this->properties[somevalue]
	
	$attributeName='';
	
	//NOTE: Showing how to use sscanf, look at some of its counterparts like sprintf
	$found = sscanf($methodName, "set%s", $attributeName);
	
	if($found>0)
	{
	    $this->properties[strtolower($attributeName)] = array_shift($args);
	    return;
	}
	
	$found = sscanf($methodName, "add%s", $attributeName);
	
	if($found>0)
	{
	    $attributeName = strtolower($attributeName);
	    if(!isset($this->properties[$attributeName]))
	    {
		$this->properties[$attributeName] = "";
	    }
	    $this->properties[$attributeName] .= " " .array_shift($args);
	    return;
	}
    }
    
    /**
     * Called by BaseForm to command this element to reload its value from
     * either GET or POST form submission, if possible.
     * @param type $formData Either the $_GET or $_POST superglobals, depending on
     * how the source form was configured.
     */
    public function handleSubmit($formData)
    {
	if(isset($formData[$this->properties['name']]))
	{
	    $this->properties['value'] = $formData[$this->properties['name']];
	}
    }
}