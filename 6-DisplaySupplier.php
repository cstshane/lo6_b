<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Update a Supplier</title>
        
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">
            <h1>Update Supplier Information</h1>

<?php
require_once __DIR__ . '/Autoloader.php';
$loader = new Autoloader();
$loader->addNamespaceMapping("\\CSTClasses_B",
        __DIR__ . '/../../private/CSTClasses_B' );
$loader->addNamespaceMapping( "\\Formitron", "classes/Formitron" );

use CSTClasses_B\DbObject;
use Formitron\BaseForm;
use Formitron\Element\Hidden;
use Formitron\Element\Text;
use Formitron\Helpers;

// Connect to the database
$db = new DbObject();

// Query the database for the supplier information
$qryResults = $db->select( "*", "Suppliers",
        "SupplierID=" . $_POST["SupplierID"] );

// There will only be one supplier selected.  Get the information for that
// supplier.
$supplierInfo = $qryResults->fetch_assoc();

// We're done with the results -- free them
$qryResults->free();

echo "<p>Supplier being updated: {$supplierInfo["CompanyName"]}</p>\n";

// Create the supplier information update form
$form = new BaseForm( BaseForm::METHOD_POST, "6-UpdateSupplier.php",
        ["id"=>"supplierInfoForm"] );

$sidHidden = new Hidden( "SupplierID", $supplierInfo["SupplierID"] );
$form->add( $sidHidden );

$companyText = new Text( "CompanyName", $supplierInfo["CompanyName"] );
$form->add( Helpers::withLabel( "CompanyName", "Company name", $companyText ) );

$contactText = new Text( "ContactName", $supplierInfo["ContactName"] );
$form->add( Helpers::withLabel( "ContactName", "Contact name", $contactText ) );

$contactTitleText = new Text( "ContactTitle", $supplierInfo["ContactTitle"] );
$form->add( Helpers::withLabel( "ContactTitle", "Contact title",
        $contactTitleText ) );

$addressText = new Text( "Address", $supplierInfo["Address"] );
$form->add( Helpers::withLabel( "Address", "Address", $addressText ) );

$cityText = new Text( "City", $supplierInfo["City"] );
$form->add( Helpers::withLabel( "City", "City", $cityText ) );

$regionText = new Text( "Region", $supplierInfo["Region"] );
$form->add( Helpers::withLabel( "Region", "Region", $regionText ) );

$pcText = new Text( "PostalCode", $supplierInfo["PostalCode"] );
$form->add( Helpers::withLabel( "PostalCode", "Postal code", $pcText ) );

$countryText = new Text( "Country", $supplierInfo["Country"] );
$form->add( Helpers::withLabel( "Country", "Country", $countryText ) );

$phoneText = new Text( "Phone", $supplierInfo["Phone"] );
$form->add( Helpers::withLabel( "Phone", "Phone", $phoneText ) );

$faxText = new Text( "Fax", $supplierInfo["Fax"] );
$form->add( Helpers::withLabel( "Fax", "Fax", $faxText ) );

$hpText = new Text( "HomePage", $supplierInfo["HomePage"] );
$form->add( Helpers::withLabel( "HomePage", "Home page", $hpText ) );

$form->add( Helpers::submitBlock( "submit", "Submit", "Reset" ) );
echo $form->render();

?>
        </div>
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </body>
</html>
