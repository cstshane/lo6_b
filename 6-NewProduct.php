<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Add a new product</title>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        </head>
    <body>
        <div class="container">
            <h1>Add a new product</h1>

<?php
// require_once __DIR__ . '/../../private/CSTClasses_B/DbObject.php';
require_once __DIR__ . '/Autoloader.php';

$loader = new Autoloader();
$loader->addNamespaceMapping("\\CSTClasses_B",
        __DIR__ . '/../../private/CSTClasses_B' );
$loader->addNamespaceMapping( "\\Formitron", "classes/Formitron" );

use CSTClasses_B\DbObject;
use Formitron\BaseForm;
use Formitron\Element\Checkbox;
use Formitron\Element\Select;
use Formitron\Element\Text;
use Formitron\Helpers;

//Open a connection to MySQL (call mysqli_connect)
$db = new DbObject();
echo "<p>Successfully connected to the database!</p>\n";

// Query the database for the suppliers
$qryResults = $db->select( "SupplierID, CompanyName", "Suppliers",
        "", "CompanyName" );

// Display the records
// dbObject::displayRecords( $qryResults );

// Build the supplier option list
// $options = array();
// while ( $row = $qryResults->fetch_row() )
// {
//     $options[$row[0]] = $row[1];
// }
$supplierOptions = dbObject::createArray( $qryResults );

// We're done with the results -- free them
$qryResults->free();

// var_dump( $optionList );

// Get the list of category IDs and names
$qryResults = $db->select( "CategoryID, CategoryName", "Categories", "",
        "CategoryName" );

// Build the category option list
$catOptions = DbObject::createArray( $qryResults );

// Free the results
$qryResults->free();

$form = new BaseForm( BaseForm::METHOD_POST, "6-AddProduct.php",
        ["id"=>"supplierForm"] );

$productText = new Text( "ProductName" );
$form->add( Helpers::withLabel( "ProductName", "Product name", $productText ) );

$supplierSelect = new Select( "SupplierID", $supplierOptions, "" );
$form->add( Helpers::withLabel( "SupplierID", "Supplier", $supplierSelect ) );

$catSelect = new Select( "CategoryID", $catOptions, "" );
$form->add( Helpers::withLabel( "CategoryID", "Category", $catSelect ) );

$qpuText = new Text( "QuantityPerUnit" );
$form->add( Helpers::withLabel( "QuantityPerUnit", "Quantity per unit",
        $qpuText ) );

$upText = new Text( "UnitPrice" );
$form->add( Helpers::withLabel( "UnitPrice", "Unit price", $upText ) );

$uisText = new Text( "UnitsInStock" );
$form->add( Helpers::withLabel( "UnitsInStock", "Units in stock", $uisText ) );

$uooText = new Text( "UnitsOnOrder" );
$form->add( Helpers::withLabel( "UnitsOnOrder", "Units on order", $uooText ) );

$rlText = new Text( "ReorderLevel" );
$form->add( Helpers::withLabel( "ReorderLevel", "Reorder level", $rlText ) );

$discCB = new Checkbox( "Discontinued", "1" );
$form->add( Helpers::withLabel( "Discontinued", "Discontinued?", $discCB ) );

$form->add( Helpers::submitBlock( "submit", "Submit", "Reset" ) );
echo $form->render();


?>
            
        </div>
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </body>
</html>
